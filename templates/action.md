---
title: {{.Type}}{{if .Action.Deprecated}} (deprecated){{end}}
---

{{if .Action.Deprecated}}{{ "{{" }}<hint warning>{{ "}}" }}{{.Type}} version {{$.Version}} has been deprecated and will be removed in the future. A newer version may be available{{ "{{" }}</hint>{{ "}}" }}{{end}}

{{.Action.Doc}}

{{if gt ($.Example | len) 0}}
{{ "{{" }}<hint info>{{ "}}" }}auto-generated example may not be a valid request. See
[`{{$.Version}}.{{$.Action.Request}}`](/protocol/structures/{{$.Version}}/{{$.Action.Request}}/)
for all fields.{{ "{{" }}</hint>{{ "}}" }}
```json
{{$.Example}}
```
{{end}}

{{if ne $.Action.Request ""}}* Request Type: [`{{$.Version}}.{{$.Action.Request}}`](/protocol/structures/{{$.Version}}/{{$.Action.Request}}/){{end}}
{{if ne $.Action.Response ""}}* Success Response Type: {{if eq $.Action.Response "String"}}`string`{{else}}[`{{$.Version}}.{{$.Action.Response}}`](/protocol/structures/{{$.Version}}/{{$.Action.Response}}/){{end}}
{{end}}{{if gt ($.Action.Errors | len) 0}}* Error response types:
{{range $error :=.Action.Errors}}  * [`{{$.Version}}.{{$error.Name}}`](/protocol/structures/{{$.Version}}/{{$error.Name}}){{if gt ($error.Doc | len) 0}} - {{$error.Doc}}{{end}}
{{end}}

{{end}}