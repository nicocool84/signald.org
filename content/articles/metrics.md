---
title: Metrics
---

signald can export metrics in Prometheus format. To enable, start signald with `--metrics` or
set the environment variable `SIGNALD_ENABLE_METRICS=true`. If signald is started using the
systemd unit file (as configured in the debian package, for example), create a file at
`/etc/default/signald` with contents:

```
SIGNALD_ENABLE_METRICS=true
```

By default the metrics listener will listen on port 9595. That can be changed with the
command line flag `--metrics-http-port` or environment variable `SIGNALD_METRICS_PORT`.